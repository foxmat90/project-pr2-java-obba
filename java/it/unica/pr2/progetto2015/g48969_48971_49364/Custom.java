package it.unica.pr2.progetto2015.g48969_48971_49364;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;
import twitter4j.*;
import twitter4j.conf.*;
import java.util.List;

/** *
 * @author 48969 Azzaro Edoardo - 48971 Palla Mattia - 49364 Piras Cesare
 *
 * <p>Cerca e restituisce gli ultimi dieci twitts che contengono una certa stringa passata a execute</p>
 */
public class Custom implements SheetFunction{
	public int dim = 10;	// Numero massimo di twitts restituiti
	public String[] risultato = new String[dim]; 	//Vettore di String , ogni elemento è un twitt trovato
	public String[] vettore = new String[5];		//Vettore dei quattro parametri utente e la stringa da ricercare in twitter

	/*
	 * Costruttore vuoto .
	 */
	public void Custom(){}


	/**
	 * Lancia la ricerca degli ultimi dieci twitts che contengono la stringa passata in args[4]
	 * @param args OggettiInIngresso args Object ... args Oggetti poi castati a String
	 * @return risultato Vettore di String[] Stringa di dimensione dieci , contiene i dieci twitts trovati
	 */
    public Object execute(Object ... args) {
		int i = 0;
		for(Object o : args){
			vettore[i] = o.toString();
			i++;
		}

		// Configurazione parametri accesso a twitter con i parametri passati in ingresso come args[0],args[1],args[2],args[3]
		//rispettivamente: Consumer key, Consumer secret, Access token e Access token secret

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		  .setOAuthConsumerKey(vettore[0])
		  .setOAuthConsumerSecret(vettore[1])
		  .setOAuthAccessToken(vettore[2])
		  .setOAuthAccessTokenSecret(vettore[3]);

			risultato[0] = "stringa non trovata";
			risultato[1] = "stringa non trovata";
			risultato[2] = "stringa non trovata";
			risultato[3] = "stringa non trovata";
			risultato[4] = "stringa non trovata";
			risultato[5] = "stringa non trovata";
			risultato[6] = "stringa non trovata";
			risultato[7] = "stringa non trovata";
			risultato[8] = "stringa non trovata";
			risultato[9] = "stringa non trovata";


		TwitterFactory tf = new TwitterFactory(cb.build());
		Twitter twitter = tf.getInstance();

		//Controlla che ci siano, e memorizza, twitts che contengono la stringa da ricercare passata come quinto elemento in args
        try {
            Query query = new Query(vettore[4]);
            QueryResult result;
			int counter = 0;
            result = twitter.search(query);
            List<Status> tweets = result.getTweets();
            for (Status tweet : tweets) {
								if (counter >= dim ) break;
									risultato[counter] = tweet.getText();
							//	if(risultato[counter].equals("")) risultato[counter] = " ";
								counter++;
            }

        } catch (TwitterException te) { // Messaggio di errore : non ha trovato nessun twitt che contenga la stringa rischiesta
            te.printStackTrace();
            System.out.println("Failed to search tweets: " + te.getMessage());
						return risultato;
						//System.exit(-1);
        }
		return risultato; // Tutti i twitts trovati
    }

    /**
	 * Restituisce la categoria della funzione Custom
	 * @return Category  String di categoria della funzione
	 */
// Metodo che restituisce la categoria di appartenenza della funzione.
	public String getCategory(){
		return "Twitter";
	}

	/**
	 * Restituisce una breve descrizione della funzione Custom
	 * @return Help, String di Help
	 */

// Metodo che restituisce una breve spiegazione della funzione.
	public String getHelp(){
		return "Visualizza gli ultimi 10 tweet con la parola chiave scelta";
	}

	/**
     * Restituisce il nome della funzione Semplice
     * @return Name, String nome della funzione
     */
	// Metodo che resituisce il nome della funzione, in questo caso non presente su LibreOffice Calc.
	public String getName(){
		return "Custom";
	}
}
