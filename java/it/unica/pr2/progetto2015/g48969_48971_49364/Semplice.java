package it.unica.pr2.progetto2015.g48969_48971_49364;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;
import java.util.ArrayList;
import java.util.Iterator;

/** *
 * @author 48969 Azzaro Edoardo - 48971 Palla Mattia - 49364 Piras Cesare
 *
 * <p>Converte la stringa passata a execute in una stringa con i caratteri in MAIUSCOLO</p>
 */
public class Semplice implements SheetFunction{

	/*
	 * Costruttore vuoto.
	 */
	public void Semplice(){}

	/**
	 * Trasforma la stringa passata in var args in una stringa formata da caratteri MAIUSCOLI
	 * @param args ... di Object in ingresso a execute
	 * @return UpperCase Stringa CON CARATTERI MAIUSCOLI
	 */
	public Object execute(Object ... args){
		String stringa = new String();
		String stringaVuota = "";
		for(Object oggetto:args){
			stringa = oggetto.toString();
		}
		if (stringa.equals(""))return "";//
		else return stringa.toUpperCase();
//return "scrivi qualcosa";
	}

	/**
	 * Restituisce la categoria della funzione Semplice
	 * @return Category  String di categoria della funzione
	 */
// Metodo che restituisce la categoria di appartenenza della funzione.
	public String getCategory(){
		return "Testo";
	}

	/**
	 * Restituisce una breve descrizione della funzione Semplice
	 * @return Help, String di Help
	 */
// Metodo che restituisce una breve spiegazione della funzione.
	public String getHelp(){
		return "Converte la stringa specificata nel campo di testo in maiuscolo.";
	}

	/**
     * Restituisce il nome della funzione Semplice
     * @return Name, String nome della funzione
     */
// Metodo che resituisce il nome della funzione su LibreOffice Calc.
	public String getName(){
		return "MAIUSC";
	}
}
